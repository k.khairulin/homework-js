var x = 'qwertyuasdfghzxcvbn';
var y = 'qweasdzxc';

function check() {
    for (var i = 0; i < y.length; i++) {
        for (var j = 0; j < x.length; j++) {
            if(j === x.length - 1 && y.charAt(i) !== x.charAt(j)){
                console.log(false);
                return;
            }else if (y.charAt(i) === x.charAt(j)) {
                j = x.length;
            }
        }
    }
    console.log(true);
}

check();

var a = 'ARTZX';
var b = '';

for(var i = 0; i < a.length; i++) {
    var symbol = a.charCodeAt(i) + 13;
    if(symbol > 90) {
        symbol = 65 + symbol - 91;
    }
    b += String.fromCharCode(symbol);
}

function createInput() {
    document.body.innerHTML += '<input id="userinput" placeholder="Specify number of elements" type="text">';
    document.body.innerHTML += '<button id="addinput" onclick="addInputs()">Add elements</button>'
}



function addInputs() {
    document.getElementById('userinput').setAttribute('name', document.getElementById('userinput').value);
    var number = document.getElementById('userinput').value;
    for(var i = 0; i < number; i++) {
        document.body.innerHTML += '<input class="listinput" type="text">';
    }
    document.body.innerHTML += '<div class="listdiv"><button onclick="addList()">Create elements</button></div>'
}



function addList() {
    var number = document.getElementById('userinput').name;
    console.log(number);
    var list = document.createElement('ul');
    list.setAttribute('id', 'myList');
    document.body.appendChild(list);
    for(var i = 0; i < number; i++ ) {
        var li = document.createElement('li');
        li.innerHTML += document.getElementsByClassName('listinput')[i].value;
        list.appendChild(li);
    }
}

function createCircleInput() {
    var circleInputDiameter = document.createElement('input');
    circleInputDiameter.setAttribute('placeholder', 'Specify Diameter');
    circleInputDiameter.setAttribute('id', 'diameter');
    var circleInputColor = document.createElement('input');
    circleInputColor.setAttribute('placeholder', 'Specify color');
    circleInputColor.setAttribute('id', 'color');
    var circleButton = document.createElement('button');
    circleButton.innerHTML += 'Create new specified circle';
    circleButton.setAttribute('id', 'newCircle');
    circleButton.setAttribute('onclick','createCircle()')
    document.body.appendChild(circleInputDiameter);
    document.body.appendChild(circleInputColor);
    document.body.appendChild(circleButton);
}

function createCircle() {
    var diameter = document.getElementById('diameter').value;
    var color = document.getElementById('color').value;
    var circle = document.createElement('div');
    circle.setAttribute('id', 'circle')
    circle.style.backgroundColor = color;
    circle.style.width = diameter + 'px';
    circle.style.height = diameter + 'px';
    circle.style.borderRadius = '50%';
    document.body.appendChild(circle);
    var deleteButton = document.createElement('button');
    deleteButton.setAttribute('onclick', 'deleteCircle()');
    deleteButton.innerHTML += 'Delete Circle';
    deleteButton.setAttribute('id', 'delete')
    document.body.appendChild(deleteButton);
    document.getElementById('createcircle').style.visibility = 'hidden';
    document.getElementById('diameter').style.visibility = 'hidden';
    document.getElementById('color').style.visibility = 'hidden';
    document.getElementById('newCircle').style.visibility = 'hidden';


}

function deleteCircle() {
    document.getElementById('circle').remove();
    document.getElementById('delete').remove();
    document.getElementById('createcircle').style.visibility = 'visible';
    document.getElementById('diameter').style.visibility = 'visible';
    document.getElementById('color').style.visibility = 'visible';
    document.getElementById('newCircle').style.visibility = 'visible';
    document.getElementById('diameter').value = '';
    document.getElementById('color').value = '';
}



